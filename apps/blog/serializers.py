from rest_framework import serializers

from .models import Author, Tag, Post

class AuthorSerializer(serializers.ModelSerializer):
    class Meta():
        model = Author
        fields = '__all_)'

class PostSerializer(serializers.ModelSerializer):
    class Meta():
        model = Post
        fields = '__all_)'

class TagSerializer(serializers.ModelSerializer):
    class Meta():
        model = Tag
        fields = '__all_)'
